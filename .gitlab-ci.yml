workflow:
  rules:
    - if: '$CI_COMMIT_TAG'
    - if: '$CI_COMMIT_BRANCH'
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event" && $CI_PROJECT_ID == $CI_MERGE_REQUEST_PROJECT_ID'
      variables:
        PIPELINE_TYPE: 'upstream'
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event" && $CI_PROJECT_ID != $CI_MERGE_REQUEST_PROJECT_ID'
      variables:
        PIPELINE_TYPE: 'fork'

stages:
  - pre_test
  - build
  - test
  - deploy

pipeline_lint:
  image: "images.paas.redhat.com/osci/rog:89d3bb52"
  stage: pre_test
  tags:
    - redhat
  script:
    - |
      echo "Fetching pipeline definition files from the commit"
      curl -o .gitlab-ci.yml "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/repository/files/.gitlab-ci.yml/raw?ref=${CI_COMMIT_SHA}" --header "PRIVATE-TOKEN: ${DIST_GIT_GATING_TESTS_LINT_TOKEN}"
      curl -o global-tasks.yml "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/repository/files/global-tasks.yml/raw?ref=${CI_COMMIT_SHA}" --header "PRIVATE-TOKEN: ${DIST_GIT_GATING_TESTS_LINT_TOKEN}"
    - |
      CI_STATUS=$(jq --null-input --arg yaml "$(<.gitlab-ci.yml)" '.content=$yaml' | curl -s --header "PRIVATE-TOKEN: ${DIST_GIT_GATING_TESTS_LINT_TOKEN}" "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/ci/lint?include_jobs=true" --header 'Content-Type: application/json' --data @- | jq '.valid')
      if [ "$CI_STATUS" = "true" ]; then
        echo ".gitlab-ci.yml is a valid GitLab pipeline"
      else
        echo "Linting failed"
        ERRORS=$(jq --null-input --arg yaml "$(<.gitlab-ci.yml)" '.content=$yaml' | curl -s --header "PRIVATE-TOKEN: ${DIST_GIT_GATING_TESTS_LINT_TOKEN}" "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/ci/lint?include_jobs=true" --header 'Content-Type: application/json' --data @- | jq '.errors')
        echo $ERRORS
        exit 1
      fi
    - |
      GLOBAL_TASKS_STATUS=$(jq --null-input --arg yaml "$(<global-tasks.yml)" '.content=$yaml' | curl -s --header "PRIVATE-TOKEN: ${DIST_GIT_GATING_TESTS_LINT_TOKEN}" "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/ci/lint?include_jobs=true" --header 'Content-Type: application/json' --data @- | jq '.valid')
      if [ "$GLOBAL_TASKS_STATUS" = "true" ]; then
        echo "global-tasks.yml is a valid GitLab pipeline"
      else
        echo "Linting failed"
        ERRORS=$(jq --null-input --arg yaml "$(<global-tasks.yml)" '.content=$yaml' | curl -s --header "PRIVATE-TOKEN: ${DIST_GIT_GATING_TESTS_LINT_TOKEN}" "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/ci/lint?include_jobs=true" --header 'Content-Type: application/json' --data @- | jq '.errors')
        echo $ERRORS
        exit 1
      fi
  rules:
    - if: '$DIST_GIT_GATING_TESTS_LINT_TOKEN'

build_image:
  stage: build
  tags:
    - redhat
    - docker
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
    - mkdir -p /kaniko/.docker
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/Dockerfile --destination $CI_REGISTRY_IMAGE/check_tickets:${CI_PIPELINE_IID}


.publish_image:
  image: quay.io/podman/stable:latest
  stage: deploy
  tags:
    - redhat
    - docker
  needs:
    - build_image
  script:
    - HOME=$CI_PROJECT_DIR podman login --username $CI_REGISTRY_USER --password $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - HOME=$CI_PROJECT_DIR podman manifest create ${CI_REGISTRY_IMAGE}/check_tickets:${IMAGE_TAG} $CI_REGISTRY_IMAGE/check_tickets:${CI_PIPELINE_IID}
    - HOME=$CI_PROJECT_DIR podman push ${CI_REGISTRY_IMAGE}/check_tickets:${IMAGE_TAG}
    - echo -e "You can retrieve this container via \e[0;102m\`podman pull ${CI_REGISTRY_IMAGE}/check_tickets:${IMAGE_TAG}\`\e[0m"

publish_container_branch:
  extends: .publish_image
  variables:
    IMAGE_TAG: '$CI_COMMIT_BRANCH'
  rules:
    - if: '$CI_COMMIT_BRANCH'

publish_container_tag:
  extends: .publish_image
  variables:
    IMAGE_TAG: '$CI_COMMIT_TAG'
  rules:
    - if: '$CI_COMMIT_TAG'

publish_container_mr:
  extends: .publish_image
  variables:
    IMAGE_TAG: 'mr$CI_MERGE_REQUEST_IID'
  rules:
    - if: '$CI_MERGE_REQUEST_IID'
