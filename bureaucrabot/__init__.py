#!/usr/bin/python3

import json
import logging
import os
import requests
import sys

from centpkg.utils import determine_rhel_state, format_current_state_message

import git as gitpython
from . import utils as bureaucrabot_utils

# Phase Identifiers
# Phase 230 is "Planning / Development / Testing" (AKA DevTestDoc)
# Phase 450 is "Stabilization"
phase_devtestdoc = 230
phase_stabilization = 450


# PROJECT_OVERRIDE is for components that do not match between CentOS Stream
# and RHEL dist-git, most notably the components that contained '+' characters
# in their names. We *MUST* be sure that distrobaker is configured to redirect
# the component syncs properly before adding an entry to this list

PROJECT_OVERRIDE = {
    "dvdplusrw-tools": "dvd+rw-tools",
    "centos-release": "redhat-release",
    "compat-sap-cplusplus": "compat-sap-c++",
    "compat-sap-cplusplus-10": "compat-sap-c++-10",
    "compat-sap-cplusplus-11": "compat-sap-c++-11",
    "compat-sap-cplusplus-12": "compat-sap-c++-12",
    "compat-sap-cplusplus-13": "compat-sap-c++-13",
    "libsigcplusplus20": "libsigc++20",
    "memtest86plus": "memtest86+",
    "perl-Text-TabsplusWrap": "perl-Text-Tabs+Wrap",
}


class BureaucraBotException(Exception):
    pass


def check_tickets(query_url, git_repo, namespace, project, oldrev, newrev, refname):
    logger = logging.getLogger(__name__)
    repo_commits = bureaucrabot_utils.get_commits(git_repo, oldrev, newrev)
    commits_data = []
    fixed = []
    for repo_commit in repo_commits:
        speclines = bureaucrabot_utils.specAdditions(project, repo_commit)
        message = bureaucrabot_utils.get_commit_message(repo_commit)

        logger.debug("Ticket data for %s: %s", repo_commit.hexsha, message + speclines)

        single_commit = {
            "hexsha": repo_commit.hexsha,
            "files": bureaucrabot_utils.get_commit_files(repo_commit),
            "resolved": bureaucrabot_utils.bugzillaIDs(
                "Resolves?:", message + speclines
            ),
            "related": bureaucrabot_utils.bugzillaIDs("Related?:", message + speclines),
            "reverted": bureaucrabot_utils.bugzillaIDs(
                "Reverts?:", message + speclines
            ),
        }
        fixed.extend(bureaucrabot_utils.bugzillaIDs("Fixes?:", message + speclines))
        commits_data.append(single_commit)

    request_data = {
        "package": project,
        "namespace": namespace,
        "ref": refname,
        "commits": commits_data,
    }

    logger.debug("Sending data to gitbz API to verify hooks result ...")
    logger.debug("Data sent: %s", json.dumps(request_data, sort_keys=True, indent=2))

    res = requests.post(query_url, json=request_data, timeout=905)

    logger.debug("Response from gitbz API: %s", res.text)
    try:
        res.raise_for_status()
    except requests.exceptions.RequestException as e:
        logger.warning("Failed ticket check for request: %s", request_data)
        logger.exception(e)
        raise BureaucraBotException("Invalid response from gitbz webservice API")

    payload = json.loads(res.text)
    if len(fixed) > 0:
        # These are bugs that were identified by the "Fixes:" prefix rather
        # than the "Resolves:" prefix.
        payload["logs"] += "\n"
        payload[
            "logs"
        ] += "*** WARNING: Detected one or more uses of 'Fixes:' instead of 'Resolves:'\n"
        payload["logs"] += "  Unverified:\n"
        for bug in fixed:
            payload["logs"] += "    {}\n".format(bug)

    return payload


def read_config(config_file):
    with open(config_file, "r") as f:
        c = json.load(f)
    return c


def log_setup():
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)

    # Log all messages into the debug log
    debug_handler = logging.FileHandler(
        "{}/debug.log".format(os.environ["CI_PROJECT_DIR"])
    )
    debug_handler.setLevel(logging.DEBUG)
    logger.addHandler(debug_handler)

    # Also log everything INFO and higher to the console
    console_handler = logging.StreamHandler()
    console_handler.setLevel(logging.INFO)
    logger.addHandler(console_handler)

    return logger


def main():
    logger = log_setup()
    cfg = read_config("/etc/bureaucrabot.json")

    logger.debug("Working Directory: {}".format(os.getcwd()))

    namespace = os.environ["CI_PROJECT_NAMESPACE"].rsplit("/")[-1]
    centos_git_repo = gitpython.Repo(path=os.environ["CI_PROJECT_DIR"])

    project = os.environ["CI_PROJECT_NAME"]
    if project in PROJECT_OVERRIDE:
        project = PROJECT_OVERRIDE[project]

    rhel_state = determine_rhel_state(
        rhel_dist_git=cfg["rhel_dist_git"],
        namespace=namespace,
        repo_name=project,
        cs_branch=os.environ["CI_MERGE_REQUEST_TARGET_BRANCH_NAME"],
        pp_api_url=cfg["api_url"],
        distrobaker_config=cfg["distrobaker_config"],
    )

    logger.info(f"{format_current_state_message(rhel_state)}")

    # If this is an unsynced package, just approve it.
    if not rhel_state.synced:
        logger.info("Auto-approving CentOS Stream-only package")
        return

    # In the rare case where a merge request includes no file changes,
    # Gitlab does not set the $CI_MERGE_REQUEST_DIFF_BASE_SHA value.
    # In this situation, we'll treat the oldrev as the immediate
    # ancestor of the newrev.
    newrev = os.environ["CI_COMMIT_SHA"]
    oldrev = os.environ.get("CI_MERGE_REQUEST_DIFF_BASE_SHA", f"{newrev}^")

    result = check_tickets(
        query_url=cfg["gitbz_query_url"],
        git_repo=centos_git_repo,
        namespace=namespace,
        project=project,
        oldrev=oldrev,
        newrev=newrev,
        refname="refs/heads/{}".format(rhel_state.rule_branch),
    )

    logger.info(result["logs"])
    if result["result"] != "ok":
        # The gitbz API sends back the literal string
        # '${z-stream MISSING_BRANCH}' instead of replacing it with the prior
        # release branch, so we'll do a find-and-replace of it here, since we
        # know what it should be.
        error_message = result["error"].replace(
            "${z-stream MISSING_BRANCH}", rhel_state.rule_branch
        )

        logger.warning(error_message)
        if rhel_state.enforcing:
            logger.debug("Enforcing mode: deny this merge request")
            sys.exit(1)
        else:
            # We specifically treat exit(77) as a nonfatal failure
            logger.debug("Non-enforcing: permit this with warnings")
            sys.exit(77)


if __name__ == "__main__":
    main()
